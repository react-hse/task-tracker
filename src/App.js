import React from "react";
import classnames from "classnames/bind";
import styles from "./App.module.scss";
import { Provider } from "react-redux";
import { connect } from "react-redux";
import { store } from "./store";
import history from "./history";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import ProjectList from "./components/projects/ProjectList";
import NewProject from "./components/projects/NewProject";
import ProjectPage from "./components/projects/ProjectPage";
import ControlPanel from "./components/controls/ControlPanel";
import { getProjects } from "./actions";

const cx = classnames.bind(styles);

const AppContainer = () => (
  <Provider store={store}>
    <ConnectedApp />
  </Provider>
);

const mapStateToProps = (state) => ({
  theme: state.theme,
  error: state.error,
});

const mapDispatchToProps = (dispatch) => ({
  loadProjects: () => dispatch(getProjects()),
});

const App = ({ theme, error, loadProjects }) => (
  <div className={cx("App", `App-theme-${theme}`)}>
    <div className={cx("App-header")}>
      <div className={cx("App-title")}>Task tracker</div>
    </div>
    {error ? (
      <h3 className={cx("Error")}>ERROR OCCURED: {JSON.stringify(error)}</h3>
    ) : null}
    <div className={cx("content")}>
      <BrowserRouter history={history}>
        <ControlPanel />
        <Switch>
          <Route exact path="/projects/new" component={NewProject} />
          <Route path="/projects/:projectId" component={ProjectPage} />
          <Route exact path="/" component={ProjectList} />
          <Route path="/">
            <h3>Page not found</h3>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
    <ProjectLoader loadProjects={loadProjects} />
  </div>
);

class ProjectLoader extends React.Component {
  componentDidMount() {
    this.props.loadProjects();
  }
  render() {
    return null;
  }
}

const ConnectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;
