import React from "react";
import classnames from "classnames/bind";
import styles from "./Project.module.scss";
import { connect } from "react-redux";
import { createProject } from "../../actions";

const cx = classnames.bind(styles);

const mapStateToProps = state => ({
  theme: state.theme
});

const mapDispatchToProps = dispatch => ({
  addProject: project => dispatch(createProject(project))
});

class NewProject extends React.Component {
  state = {
    name: ""
  };

  handleChange = event => {
    this.setState({ name: event.target.value });
  };

  handleSave = () => {
    const name = this.state.name;

    if (name === "") {
      alert("Project name is empty");
      return;
    }

    this.props.addProject({ name: name });

    this.props.history.push("/");
  };

  render() {
    const inputStyle = cx(
      "editor-field",
      `editor-field-theme-${this.props.theme}`
    );

    return (
      <div>
        <h2>Create project</h2>
        <div className={cx("spaced")}>
          <input
            className={inputStyle}
            required
            id="name"
            type="text"
            size="35"
            value={this.state.name}
            placeholder="Enter project name..."
            onChange={this.handleChange}
          />
        </div>
        <button
          type="submit"
          className={cx("button", "button-success")}
          onClick={this.handleSave}
        >
          Create project
        </button>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProject);
