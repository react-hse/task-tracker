import React from "react";
import NewTask from "../tasks/NewTask";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import TaskPage from "../tasks/TaskPage";

const mapStateToProps = state => ({
  projects: state.projects,
  tasks: state.tasks
});

const ProjectPage = ({ projects, tasks, match }) => {
  const projectId = parseInt(match.params.projectId);
  const project = projects.find(p => p.id === projectId);

  if (project === undefined) {
    return <div>Project [id: {projectId}] not found</div>;
  }

  const projectTasks = tasks[projectId];

  return (
    <div>
      <h2>Project '{project.name}'</h2>
      <TaskPage projectId={projectId} tasks={projectTasks} />
      <NewTask project={project} />
    </div>
  );
};

export default connect(mapStateToProps, null)(withRouter(ProjectPage));
