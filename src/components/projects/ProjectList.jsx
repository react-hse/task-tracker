import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import classnames from "classnames/bind";
import styles from "./Project.module.scss";

const cx = classnames.bind(styles);

const mapStateToProps = state => ({
  theme: state.theme,
  projects: state.projects,
  tasks: state.tasks
});

const ProjectList = ({ projects, theme, tasks }) => (
  <div>
    <h2>Projects</h2>
    <div>
      {projects.map(p => (
        <ProjectCard
          theme={theme}
          key={p.id}
          project={p}
          taskNumber={tasks[p.id] ? tasks[p.id].length : 0}
        />
      ))}
    </div>
    <Link
      className={cx("project-link", `project-link-theme-${theme}`)}
      to="/projects/new"
    >
      Add new project
    </Link>
  </div>
);

const ProjectCard = ({ project, taskNumber }) => (
  <div>
    <Link
      className={cx("project-link", "project-link-primary")}
      to={`/projects/${project.id}`}
    >
      {project.name}
      <div>{taskNumber} tasks</div>
    </Link>
  </div>
);

export default connect(mapStateToProps, null)(ProjectList);
