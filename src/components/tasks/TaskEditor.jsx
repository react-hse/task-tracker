import React from "react";
import { connect } from "react-redux";
import { createTask } from "../../actions";

import { PRIORITIES } from "../../utils";

import classnames from "classnames/bind";
import styles from "./Task.module.scss";
const cx = classnames.bind(styles);

const mapStateToProps = (state) => ({
  theme: state.theme,
});

const mapDispatchToProps = (dispatch) => ({
  addTask: (projectId, task) => dispatch(createTask(projectId, task)),
});

class TaskEditor extends React.Component {
  state = { name: "", description: "", priority: 1 };

  handleSave = () => {
    const name = this.state.name;
    const description = this.state.description;
    const priority = this.state.priority;

    if (name === "" || description === "") {
      alert("Task name or description is empty");
      return;
    }

    const task = {
      name: name,
      description: description,
      priority: parseInt(priority),
    };

    this.props.addTask(this.props.projectId, task);
    this.setState({ name: "", description: "" });
    this.props.onCancelClick();
  };

  handleChange = (event) => {
    this.setState({ [event.target.id]: event.target.value });
  };

  render() {
    const theme = this.props.theme;
    const inputStyle = cx("editor-field", `editor-field-theme-${theme}`);
    const task = this.state;
    return (
      <div>
        <div className={cx("spaced")}>
          <input
            className={inputStyle}
            required
            id="name"
            type="text"
            size="35"
            value={task.name}
            placeholder="Enter task name..."
            onChange={this.handleChange}
          />
        </div>

        <div className={cx("spaced")}>
          <select
            className={inputStyle}
            id="priority"
            onChange={this.handleChange}
          >
            {PRIORITIES.map((p, i) => (
              <option key={p.label} value={i + 1}>
                {p.label}
              </option>
            ))}
          </select>
        </div>
        <div className={cx("spaced")}>
          <textarea
            className={inputStyle}
            required
            id="description"
            cols="50"
            rows="10"
            placeholder="Enter description..."
            value={this.state.description}
            onChange={this.handleChange}
          ></textarea>
        </div>
        <button
          className={cx("button", "button-danger")}
          onClick={this.props.onCancelClick}
        >
          Cancel
        </button>
        <button
          type="submit"
          className={cx("button", "button-success")}
          onClick={this.handleSave}
        >
          Create task
        </button>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskEditor);
