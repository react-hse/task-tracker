import React from "react";

import TaskCard from "./TaskCard";

import classnames from "classnames/bind";
import styles from "./Task.module.scss";
const cx = classnames.bind(styles);

const TaskBoard = ({ tasks }) => (
  <div className={cx("spaced")}>
    <div className={cx("task-board")}>
      {tasks.map(task => (
        <TaskCard key={task.id} task={task} />
      ))}
    </div>
  </div>
);

export default TaskBoard;
