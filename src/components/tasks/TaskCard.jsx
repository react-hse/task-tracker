import React from "react";
import { connect } from "react-redux";

import PriorityBadge from "./PriorityBadge";

import classnames from "classnames/bind";
import styles from "./Task.module.scss";
import { updateTask } from "../../actions";
import { withRouter } from "react-router-dom";
const cx = classnames.bind(styles);

const mapDispatchToProps = (dispatch) => ({
  updateTask: (projectID, task) => dispatch(updateTask(projectID, task)),
});

const TaskCard = ({ task, match, updateTask }) => {
  const projectID = match.params.projectId;
  return (
    <div className={cx("task-card")}>
      <div className={cx("task-header")}>
        <span className="task-title">
          #{task.id} | {task.name}
        </span>
        <PriorityBadge priority={task.priority} />
      </div>

      <button
        className={cx("switch", `switch-${task.completed}`)}
        onClick={() => {
          updateTask(projectID, { ...task, completed: !task.completed });
        }}
      ></button>

      <div className={cx("task-content")}>
        <div className={cx("task-description")}>{task.description}</div>
      </div>
    </div>
  );
};

export default withRouter(connect(null, mapDispatchToProps)(TaskCard));
