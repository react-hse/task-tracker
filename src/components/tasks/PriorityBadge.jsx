import React from "react";

import { PRIORITIES } from "../../utils";

import classnames from "classnames/bind";
import styles from "./Task.module.scss";
const cx = classnames.bind(styles);

const PriorityBadge = ({ priority }) => (
  <div
    className={cx(
      "priority-badge",
      `priority-badge-style-${PRIORITIES[priority - 1].style}`
    )}
  >
    {PRIORITIES[priority - 1].label}
  </div>
);

export default PriorityBadge;
