import React from "react";
import { withRouter } from "react-router-dom";
import TaskEditor from "./TaskEditor";

import classnames from "classnames/bind";
import styles from "./Task.module.scss";
const cx = classnames.bind(styles);

class NewTask extends React.Component {
  state = { showEditor: false };

  showEditor = () => {
    this.setState({ showEditor: true });
  };

  hideEditor = () => {
    this.setState({ showEditor: false });
  };

  render() {
    const showEditor = this.state.showEditor;
    const project = this.props.project;

    if (showEditor) {
      return (
        <span>
          <h3>Create new task for '{project.name}'</h3>
          <TaskEditor projectId={project.id} onCancelClick={this.hideEditor} />
        </span>
      );
    }

    return (
      <div>
        <button
          className={cx("button", "button-primary")}
          onClick={this.showEditor}
        >
          Add new task
        </button>
      </div>
    );
  }
}

export default withRouter(NewTask);
