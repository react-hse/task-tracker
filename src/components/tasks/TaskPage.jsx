import React from "react";
import { connect } from "react-redux";

import TaskBoard from "./TaskBoard";
import { SORT } from "../../utils";
import { sortTasksAction } from "../../actions";

import classnames from "classnames/bind";
import styles from "./Task.module.scss";
const cx = classnames.bind(styles);

const mapStateToProps = (state) => ({
  theme: state.theme,
});

const mapDispatchToProps = (dispatch) => ({
  sortTasks: (projectId, field, direction) =>
    dispatch(sortTasksAction(projectId, field, direction)),
});

class TaskPage extends React.Component {
  state = {
    sortDirection: SORT.DIRECTION.ASCENDING,
    sortField: SORT.FIELD.ID,
  };

  handleSortIdClick = () => {
    const sortField = this.state.sortField;
    const sortDirection = this.state.sortDirection;

    const strategy =
      sortField === SORT.FIELD.ID && sortDirection === SORT.DIRECTION.ASCENDING
        ? SORT.DIRECTION.DESCENDING
        : SORT.DIRECTION.ASCENDING;

    this.setState({
      sortField: SORT.FIELD.ID,
      sortDirection: strategy,
    });

    this.props.sortTasks(this.props.projectId, SORT.FIELD.ID, strategy);
  };

  handleSortPriorityClick = () => {
    const sortField = this.state.sortField;
    const sortDirection = this.state.sortDirection;

    const strategy =
      sortField === SORT.FIELD.PRIORITY &&
      sortDirection === SORT.DIRECTION.ASCENDING
        ? SORT.DIRECTION.DESCENDING
        : SORT.DIRECTION.ASCENDING;

    this.setState({
      sortField: SORT.FIELD.PRIORITY,
      sortDirection: strategy,
    });
    this.props.sortTasks(this.props.projectId, SORT.FIELD.PRIORITY, strategy);
  };

  render() {
    const tasks = this.props.tasks;
    if (tasks.length === 0) {
      return <h3>No tasks yet</h3>;
    }

    return (
      <div>
        <h3>Tasks</h3>
        <button
          className={cx("button", "button-dark")}
          onClick={this.handleSortIdClick}
        >
          ↑↓ id
        </button>
        <button
          className={cx("button", "button-secondary")}
          onClick={this.handleSortPriorityClick}
        >
          ↑↓ priority
        </button>
        <TaskBoard tasks={tasks} />
      </div>
    );
  }
}

TaskPage.defaultProps = {
  tasks: [],
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskPage);
