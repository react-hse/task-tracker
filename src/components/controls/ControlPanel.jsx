import React from "react";
import { withRouter } from "react-router-dom";
import classnames from "classnames/bind";
import styles from "./Control.module.scss";
import { connect } from "react-redux";
import { changeThemeAction } from "../../actions";
import { THEME } from "../../utils";

const cx = classnames.bind(styles);

const mapStateToProps = (state) => ({
  theme: state.theme,
});

const mapDispatchToProps = (dispatch) => ({
  changeTheme: () => dispatch(changeThemeAction),
});

const ControlPanel = ({
  history,
  theme,
  changeTheme,
}) => (
  <div className={cx("nav-panel")}>
    <button
      title="Go back"
      onClick={history.goBack}
      className={cx("nav-item", "button", "button-primary")}
    >
      ← Go back
    </button>

    <button
      title="Change theme"
      className={cx("nav-item", "button", "Theme-switcher", [
        `Theme-switcher-theme-${theme}`,
      ])}
      onClick={changeTheme}
    >
      <span role="img" aria-label="Change theme">
        {theme === THEME.LIGHT ? "☼" : "☾"} Change theme
      </span>
    </button>

    <button
      title="List projects"
      onClick={() => history.push("/")}
      className={cx("nav-item", "button")}
    >
      <span role="img" aria-label="Go to projects">
        🗊 Go to projects
      </span>
    </button>

    <button
      title="Create new project"
      onClick={() => history.push("/projects/new")}
      className={cx("nav-item", "button", "button-success")}
    >
      + Add project
    </button>
  </div>
);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ControlPanel));
