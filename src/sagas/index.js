import { call, put, takeLatest, all } from "redux-saga/effects";
import {
  GET_PROJECTS,
  loadProjectsAction,
  showError,
  CREATE_PROJECT,
  GET_TASKS,
  CREATE_TASK,
  loadProjectTasksAction,
  getTasks,
  getProjects,
  UPDATE_TASK,
  sortTasksAction,
} from "../actions";
import { Projects, Tasks } from "../endpoints";

export function* watchGetProjects() {
  yield takeLatest(GET_PROJECTS, callGetProjects);
}

function* callGetProjects() {
  try {
    const response = yield call(Projects.index);
    yield put(loadProjectsAction(response));
    for (let i = 0; i < response.length; i++) {
      let tasks = yield call(Tasks.index, parseInt(response[i].id));
      yield put(loadProjectTasksAction(response[i].id, tasks));
    }
  } catch (error) {
    yield put(showError(error));
  }
}

// CREATE PROJECT
export function* watchCreateProject() {
  yield takeLatest(CREATE_PROJECT, callCreateProject);
}

function* callCreateProject(action) {
  try {
    const project = action.payload;
    yield call(Projects.create, project);
    yield put(getProjects());
  } catch (error) {
    yield put(showError(error));
  }
}

// GET TASKS
export function* watchGetTasks() {
  yield takeLatest(GET_TASKS, callGetTasks);
}

function* callGetTasks(action) {
  try {
    const projectID = action.payload;
    const response = yield call(Tasks.index, projectID);
    yield put(loadProjectTasksAction(projectID, response));
    yield put(sortTasksAction(projectID));
  } catch (error) {
    yield put(showError(error));
  }
}

// CREATE TASK
export function* watchCreateTask() {
  yield takeLatest(CREATE_TASK, callCreateTask);
}

function* callCreateTask(action) {
  try {
    const { projectID, task } = action.payload;
    yield call(Tasks.create, projectID, task);
    yield put(getTasks(projectID));
  } catch (error) {
    yield put(showError(error));
  }
}

// UPDATE TASK
export function* watchUpdateTask() {
  yield takeLatest(UPDATE_TASK, callUpdateTask);
}

function* callUpdateTask(action) {
  try {
    const { projectID, task } = action.payload;
    task.projectId = parseInt(projectID);
    yield call(Tasks.update, projectID, task);
    yield put(getTasks(projectID));
  } catch (error) {
    yield put(showError(error));
  }
}

export default function* rootSaga() {
  yield all([
    call(watchCreateProject),
    call(watchCreateTask),
    call(watchGetProjects),
    call(watchGetTasks),
    call(watchUpdateTask),
  ]);
}
