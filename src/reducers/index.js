import {
  SORT_TASKS,
  LOAD_PROJECTS,
  CHANGE_THEME,
  SHOW_ERROR,
  HIDE_ERROR,
  LOAD_PROJECT_TASKS,
} from "../actions";
import { THEME, customSort } from "../utils";

const defaultState = {
  projects: [],
  tasks: {},
  theme: THEME.LIGHT,
  error: "",
};

const rootReducer = (state = defaultState, action) => {
  console.log(action);
  switch (action.type) {
    case SHOW_ERROR: {
      return {
        ...state,
        error: action.payload.message,
      };
    }
    case HIDE_ERROR: {
      return {
        ...state,
        error: "",
      };
    }

    case SORT_TASKS: {
      const { projectId, field, direction } = action.payload;

      const projectTasks = state.tasks[projectId].concat();

      const sortedTasks = customSort(projectTasks, direction, field);

      const tasks = Object.assign({}, state.tasks);
      tasks[projectId] = sortedTasks;

      return {
        ...state,
        tasks: tasks,
      };
    }

    case LOAD_PROJECTS: {
      return {
        ...state,
        projects: action.payload,
      };
    }

    case LOAD_PROJECT_TASKS: {
      const { projectID, tasks } = action.payload;
      const allTasks = Object.assign({}, state.tasks);
      allTasks[projectID] = tasks;
      return {
        ...state,
        tasks: allTasks,
      };
    }

    case CHANGE_THEME: {
      let newTheme = state.theme === THEME.LIGHT ? THEME.DARK : THEME.LIGHT;
      return {
        ...state,
        theme: newTheme,
      };
    }

    default:
      return state;
  }
};

export default rootReducer;
