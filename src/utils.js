export const THEME = {
  LIGHT: "light",
  DARK: "dark",
};

export const PRIORITIES = [
  { label: "LOW", style: "secondary" },
  { label: "MEDIUM", style: "primary" },
  { label: "HIGH", style: "danger" },
];

export const SORT = {
  DIRECTION: { ASCENDING: 1, DESCENDING: -1 },
  FIELD: { ID: "id", PRIORITY: "priority" },
};

export function customSort(
  items,
  direction = SORT.DIRECTION.ASCENDING,
  field = SORT.FIELD.ID
) {
  return items.sort((a, b) => {
    return (a[field] - b[field]) * direction;
  });
}
