const GET = "GET";
const POST = "POST";
const PUT = "PUT";

function request(url, method, body) {
  return fetch(process.env.REACT_APP_BACKURL + url, {
    body: JSON.stringify(body),
    method: method,
    headers: {
      "Content-Type": "application/json",
      Token: process.env.REACT_APP_AUTH_CREDENTIALS,
    },
  }).then((res) => {
    if (!res.ok) {
      throw new Error(res.status);
    }
    return res;
  });
}

// GET has no body
function get(url) {
  return request(url, GET);
}

function post(url, body) {
  return request(url, POST, body);
}

function put(url, body) {
  return request(url, PUT, body);
}

export { get, post, put };
