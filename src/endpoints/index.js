import { get, post, put } from "../config/api";

export const Projects = {
  index: () => get("/projects/").then((r) => r.json()),
  single: (id) => get(`/projects/${id}/`),
  create: (params) => post("/projects/", params),
  update: (id, params) => put(`/projects/${id}/`, params),
};

export const Tasks = {
  index: (pID) => get(`/projects/${pID}/tasks/`).then((r) => r.json()),
  single: (pID, id) => get(`/projects/${pID}/tasks/${id}/`),
  create: (pID, params) => post(`/projects/${pID}/tasks/`, params),
  update: (pID, task) => put(`/projects/${pID}/tasks/${task.id}/`, task),
};
