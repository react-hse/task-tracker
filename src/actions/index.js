// SHOW ERROR
export const SHOW_ERROR = "SHOW_ERROR";
export const showError = (error) => ({
  type: SHOW_ERROR,
  payload: error,
});

export const HIDE_ERROR = "HIDE_ERROR";
export const hideError = () => ({
  type: HIDE_ERROR,
});


// GET PROJECTS
export const LOAD_PROJECTS = "LOAD_PROJECTS";
export const loadProjectsAction = (projects) => ({
  type: LOAD_PROJECTS,
  payload: projects,
});

export const GET_PROJECTS = "GET_PROJECTS";
export const getProjects = () => ({
  type: GET_PROJECTS,
});


// CREATE PROJECT
export const CREATE_PROJECT = "CREATE_PROJECT";
export const createProject = (project) => ({
  type: CREATE_PROJECT,
  payload: project,
});

// GET TASKS
export const LOAD_PROJECT_TASKS = "LOAD_PROJECT_TASKS";
export const loadProjectTasksAction = (projectID, tasks) => ({
  type: LOAD_PROJECT_TASKS,
  payload: { projectID: projectID, tasks: tasks },
});

export const GET_TASKS = "GET_TASKS";
export const getTasks = (projectID) => ({
  type: GET_TASKS,
  payload: projectID,
});


// CREATE TASK
export const CREATE_TASK = "CREATE_TASK";
export const createTask = (projectID, task) => ({
  type: CREATE_TASK,
  payload: {
    projectID: projectID,
    task: task,
  },
});

// UPDATE TASK
export const UPDATE_TASK = "UPDATE_TASK";
export const updateTask = (projectID, task) => ({
  type: UPDATE_TASK,
  payload: {
    projectID: projectID,
    task: task,
  },
});

// SORT TASKS
export const SORT_TASKS = "SORT_TASKS";
export const sortTasksAction = (projectId, field, direction) => ({
  type: SORT_TASKS,
  payload: { projectId: projectId, field: field, direction: direction },
});

// CHANGE THEME
export const CHANGE_THEME = "CHANGE_THEME";
export const changeThemeAction = {
  type: CHANGE_THEME,
};
